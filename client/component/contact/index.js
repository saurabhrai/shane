import React, { Component } from 'react';
import ContactForm from './_form';
import { isEmail } from 'validator';
import { isEmpty } from 'lodash';
import axios from 'axios';
import SnackBarNotification from '../extras/snackbar';
import loader from "../../../public/assets/images/loader.gif"

class Contact extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            file: "",
            description: "",
            errors: {},
            message: "",
            color: "inherit",
            open: false,
            showLoading: false,
            allowedExtensions: ["image/png", "image/jpeg", "application/pdf"]
        }

        this.handleChangeEvent = this.handleChangeEvent.bind(this);
        this.handleSubmitEvent = this.handleSubmitEvent.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }


    handleChangeEvent(e) {
        let name = e.target.name;
        let value = e.target.value;
        let errors = {};

        if (name === "email") {
            if (isEmpty(value)) {
                errors.email = "Email cannot be blank"
            } else if (!isEmail(value)) {

                errors.email = "Invalid email"
            } else {
                errors.email = "";
            }
        }

        if (name === "file") {
            let file = e.target.files[0];
            let fileReader = new FileReader();
            if (this.state.allowedExtensions.indexOf(file.type) > -1) {
                fileReader.onload = () => {
                    this.setState({ file })
                }
                fileReader.readAsDataURL(file)
                errors.file = ""
            } else {
                errors.file = "Only PNG, JPEG or PDF are allowed!"
            }
        }

        if (name === "name") {
            if (isEmpty(value)) {
                errors.name = "Name cannot be blank"
            } else {
                errors.name = "";
            }
        }

        if (name === "description") {
            if (isEmpty(value)) {
                errors.description = "Description cannot be blank"
            } else {
                errors.description = "";
            }
        }

        this.setState({
            [name]: value,
            errors: { ...this.state.errors, ...errors }
        })
    }



    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ open: false });
    };

    handleSubmitEvent() {

        let { email, description, name, file } = this.state;
        let errors = {};

        this.setState({ showLoading: true })

        if (isEmpty(email)) {
            errors.email = "Email cannot be blank"
        } else if (!isEmail(email)) {
            errors.email = "Invalid email"
        } else {
            delete errors.email;
        }

        if (isEmpty(name)) {
            errors.name = "Name cannot be blank"
        } else {
            delete errors.name;
        }



        if (typeof file !== "object") {
            errors.file = "File cannot be blank"
        } else if (this.state.allowedExtensions.indexOf(file.type) < 0) {
            errors.file = "Only PNG, JPEG or PDF are allowed!"
        } else {
            delete errors.file;
        }

        if (isEmpty(description)) {
            errors.description = "Description cannot be blank"
        } else {
            delete errors.description;
        }

        if (isEmpty(errors)) {
            const formData = new FormData();
            formData.append('file', file);
            formData.append('name', name);
            formData.append('email', email);
            formData.append('description', description);
            axios.post(`/api/upload-file`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(response => {
                document.getElementById("contact-form-data-cfd").reset()
                this.setState({
                    showLoading: false,
                    open: true,
                    color: "inherit",
                    message: "Email send successfully!",
                    name: "",
                    email: "",
                    description: ""
                })
            }).catch(error => {
                this.setState({
                    showLoading: false,
                    open: true,
                    color: "secondary",
                    message: "Something went wrong!"
                })
            });
        } else {
            this.setState({ errors })
        }



    }

    render() {

        let loadingImgStyle = {
            position: "absolute",
            width: "100px",
            left: "0",
            right: "0",
            margin: "auto",
            top: "45%",
        }

        const containerStyle = {
            position: "absolute",
            zIndex: "999999999999",
            left: "0",
            right: "0",
            margin: "auto",
            background: "#1513130f",
            height: "100%"
        }

        return (
            <React.Fragment>
                {this.state.showLoading && (
                    <div style={containerStyle}>
                        <img style={loadingImgStyle} src={loader} />
                    </div>
                )}
                <SnackBarNotification color={this.state.color} message={this.state.message} isOpen={this.state.open} handleClose={this.handleClose} />
                <ContactForm {...this} />
            </React.Fragment>

        )
    }
}

export default Contact;