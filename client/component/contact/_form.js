import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import { Typography } from "@material-ui/core";
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import { isEmpty } from 'lodash';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 350,
    },
    description: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 750,
    },
    dense: {
        marginTop: 19,
    },
    root: {
        flexGrow: 1,
    },
    file: {
        width: '350px',
        marginLeft: '9px',
        marginTop: '28px',
        textAlign:"left"
    },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: 'center',
        height: '100vh',
        color: theme.palette.text.secondary,
    },
});


const ContactForm = ({ state, handleChangeEvent, classes, handleSubmitEvent }) => {

    let { name, email, description, errors } = state;

    return (

        <div className={classes.root}>
            <Paper className={classes.paper}>
                <Grid container spacing={24} justify="center">
                    <AppBar position="static" color="default">
                        <Toolbar>
                            <Typography variant="h6" color="inherit">
                                Contact Form
                                </Typography>
                        </Toolbar>
                    </AppBar>
                    <Grid item xs={8}>
                        <form className={classes.container} noValidate autoComplete="off" id="contact-form-data-cfd">
                            <TextField
                                required
                                error={!isEmpty(errors.name)}
                                id="standard-name"
                                label="Name"
                                name="name"
                                onChange={(e) => handleChangeEvent(e)}
                                className={classes.textField}
                                margin="normal"
                                value={name}
                            />


                            <TextField
                                required
                                error={!isEmpty(errors.email)}
                                id="standard-required"
                                label="Email"
                                name="email"
                                className={classes.textField}
                                onChange={(e) => handleChangeEvent(e)}
                                margin="normal"
                                value={email}
                            />

                            <TextField
                                id="standard-textarea"
                                error={!isEmpty(errors.description)}
                                label="Description"
                                placeholder="Placeholder"
                                name="description"
                                multiline
                                className={classes.description}
                                onChange={(e) => handleChangeEvent(e)}
                                value={description}
                                margin="normal"
                            />

                            <div  className={classes.file}>
                           
                                <input
                                    
                                    type="file"
                                    name="file"
                                    onChange={(e) => handleChangeEvent(e)}
                                    style={{width:"100%", marginBottom:"7px", border:errors.file ? "2px solid red" : ""}}
                                   
                                />
                                 { errors.file && (<span style={{ color: "red", }}>{errors.file}</span>)}
                            </div>

                           

                        </form>

                        <Button onClick={() => handleSubmitEvent()} justify="left" style={{ float: 'right' }} variant="contained" color="default" className={classes.button}>
                            Send
                                </Button>
                    </Grid>

                </Grid>
            </Paper>
        </div >

    )
}




export default withStyles(styles)(ContactForm);