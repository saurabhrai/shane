import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
    close: {
        padding: theme.spacing.unit / 2,
    },
});

const SnackBarNotification = ({ classes, isOpen, message, handleClose, color }) => {

    return (
        <div>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                open={isOpen}
                autoHideDuration={6000}
                onClose={() => handleClose()}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">{message}</span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color={color}
                        className={classes.close}
                        onClick={() => handleClose()}
                    >
                        <CloseIcon />
                    </IconButton>,
                ]}
            />
        </div>
    );
}


export default withStyles(styles)(SnackBarNotification);