import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Contact from './component/contact';

export default () => (
    <Router>
        <Switch>
            <Route  path="*" component={Contact} />
        </Switch>
    </Router>
)