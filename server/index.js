const express = require('express');
const bodyParser = require('body-parser');
const Path = require('path');

const api = require("./api");

const PORT = 7000;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));

app.use('/api', api)
app.use(express.static('public/build/'))

app.get("*", (req, res) => {
    res.sendFile(Path.resolve('public/build/app.html'))
})

app.listen(PORT, () => {
    console.log(`Application is listening to PORT ${PORT}`)
})
