const express = require("express");
const router = express.Router();
const AWS = require('aws-sdk');
const fs = require('fs');
const fileType = require('file-type');
const bluebird = require('bluebird');
const multiparty = require('multiparty');
const mailer = require("./mailer/config")
const pathh = require("path");

// configure the keys for accessing AWS
AWS.config.update({
  accessKeyId: 'AKIAY45G2LHDM6OTRTX3',
  secretAccessKey: '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'
});

// configure AWS to work with promises
AWS.config.setPromisesDependency(bluebird);

// create S3 instance
const s3 = new AWS.S3();

// abstracts function to upload a file returning a promise
const uploadFile = (buffer, name, type) => {
  const params = {
    ACL: 'public-read',
    Body: buffer,
    Bucket: 'shane-demo',
    ContentType: type.mime,
    Key: `${name}.${type.ext}`
  };
  return s3.upload(params).promise();
};

router.post('/upload-file', (request, response) => {
  const form = new multiparty.Form();
  form.parse(request, async (error, fields, files) => {
    if (error) {
      return response.status(400).send(error);
    } else {
      try {
        const name = fields['name'][0]
        const email = fields['email'][0]
        const description = fields['description'][0]
        const path = files.file[0].path;
        const buffer = fs.readFileSync(path);
        const type = fileType(buffer);
        const timestamp = Date.now().toString();
        const fileName = `data/${timestamp}-file`;
        const data = await uploadFile(buffer, fileName, type);

        fs.readFile(pathh.resolve("server/mailer") + "/template/template.html", "utf8", (err, htmlData) => {
          if (err) {
            return response.status(400).send(error);
          }  else {
            let replaceHtml = htmlData.replace("<%= name %>", name)
            replaceHtml = replaceHtml.replace("<%= email %>", email)
            replaceHtml = replaceHtml.replace("<%= description %>", description)
            const emailData = {
              to: email,
              from: "saurabh.rai900@gmail.com",
              subject: 'Contact Query',
              attachments: [{
                filename: (data.key).replace("data/", ""),
                path: data.Location
              }],
              html: replaceHtml
            };

            mailer.sendMail(emailData, (err, res) => {
              if (err) {
                console.log(err)
                return response.status(400).send(error);
              } else {
                return response.status(200).send(data);
              }
            });
          }

        })
      } catch (error) {
        return response.status(400).send(error);
      }
    }

  });
});

module.exports = router;