const webpack = require("webpack");
const path = require("path");
const APPDIR = path.resolve("client");
const PUBLICDIR = path.resolve("public");
const BUILDDIR = path.resolve("public/build");
const HtmlWebpackPlugin = require('html-webpack-plugin');


const config = {
    entry: [APPDIR + "/index.js"],
    output: {
        path: BUILDDIR,
        filename: "[name].[hash].js",
    },
    mode: "development",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ["react", "env", "stage-2"]
                }
            },
            {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader']
            },
            {
                test: /\.(jpg|png|svg|woff2|woff|ttf|eot|gif)$/,
                loader: "file-loader"
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: PUBLICDIR + "/template/sample.html",
            path: BUILDDIR,
            filename: "app.html"
        })
    ]
}

module.exports = config;